import { Col, Image, Layout, Row, Spin, Modal, AutoComplete } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';
import './App.css';
import { ColorBaseGrayEnum } from './color';

const BASE_URL = 'https://www.omdbapi.com';
const errorImage =
	'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg==';

const { Header, Content } = Layout;

const App = () => {
	const [isModalVisible, setIsModalVisible] = useState({
		isShowModal: false,
		data: null,
	});
	const { isShowModal, data } = isModalVisible;
	const [isLoadingSpin, setIsLoadingSpin] = useState(false);
	const [page, setPage] = useState(1);
	const [dataListMovies, setDataListMovies] = useState([]);
	const [options, setOptions] = useState([]);

	const fetchListMovie = useCallback(
		async ({ page, reset = false }) => {
			setIsLoadingSpin(true);
			const response = await axios
				.get(`${BASE_URL}/?apikey=c5df440b&s=Batman&page=${page}`)
				.then((res) => res)
				.catch((err) => {
					setIsLoadingSpin(false);
				});

			if (response.data.Response === 'True') {
				if (reset) {
					const tempDataListMovie = response.data.Search;

					const optionsTemp = [];

					tempDataListMovie.forEach((item) => {
						optionsTemp.push({
							value: item.Title,
							label: item.Title,
						});
					});

					setDataListMovies(tempDataListMovie);
					setOptions(optionsTemp);
				} else {
					const tempDataListMovie = (dataListMovies || []).concat(
						response.data.Search
					);

					const optionsTemp = [];

					tempDataListMovie.forEach((item) => {
						optionsTemp.push({
							value: item.Title,
							label: item.Title,
						});
					});

					setDataListMovies(tempDataListMovie);
					setOptions(optionsTemp);
				}

				setIsLoadingSpin(false);
			} else {
				window.alert('Gagal memuat data');
				setIsLoadingSpin(false);
			}
		},
		[dataListMovies]
	);

	const handleScroll = useCallback(() => {
		const windowHeight =
			'innerHeight' in window
				? window.innerHeight
				: document.documentElement.offsetHeight;
		const body = document.body;
		const html = document.documentElement;
		const docHeight = Math.max(
			body.scrollHeight,
			body.offsetHeight,
			html.clientHeight,
			html.scrollHeight,
			html.offsetHeight
		);
		const windowBottom = windowHeight + window.pageYOffset;

		if (windowBottom >= docHeight) {
			fetchListMovie({ page: page + 1 });
			setPage(page + 1);
		}
	}, [fetchListMovie, page]);

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);

		return () => {
			window.removeEventListener('scroll', handleScroll);
		};
	}, [handleScroll]);

	useEffect(() => {
		fetchListMovie({});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<Layout style={{ backgroundColor: 'white' }}>
			<Row style={{ justifyContent: 'center' }}>
				<Col style={{ maxWidth: 425 }}>
					<Header style={{ backgroundColor: 'white', height: 30 }}>
						<p style={{ textAlign: 'center' }}>
							Movie List Create by{' '}
							<span>
								<a
									href='https://www.linkedin.com/in/wahyu-fatur-rizky/'
									rel='noreferrer'
									target='_blank'>
									Wahyu Fatur Rizki
								</a>
							</span>
						</p>
					</Header>
					<Content>
						<div
							style={{
								width: 320,
								paddingRight: 14,
								paddingLeft: 14,
								paddingTop: 14,
							}}>
							<AutoComplete
								allowClear={true}
								style={{ width: '100%', marginTop: 12, marginBottom: 12 }}
								options={options}
								filterOption={(inputValue, option) =>
									option.value
										.toUpperCase()
										.indexOf(inputValue.toUpperCase()) !== -1
								}
								onSelect={(data) => {
									let tempSelect = dataListMovies.filter(
										(filtering) => filtering.Title === data
									);

									if (!tempSelect) {
										fetchListMovie({});
									} else {
										setDataListMovies(tempSelect);
									}
								}}
								onSearch={(data) => {
									if (!data) {
										fetchListMovie({ reset: true });
									}
								}}
								placeholder='cari film, contoh: Batman'
							/>
							{dataListMovies.map((data, index) => {
								return (
									<div
										key={index}
										onClick={() =>
											setIsModalVisible({ isShowModal: true, data: data })
										}
										style={{
											borderRadius: 6,
											marginBottom: 14,
											border: `1px solid ${ColorBaseGrayEnum.gray400}`,
											cursor: 'pointer',
										}}>
										<Row>
											<Col>
												<Image
													preview={false}
													width={120}
													height='100%'
													style={{
														borderTopLeftRadius: 6,
														borderBottomLeftRadius: 6,
														objectFit: 'cover',
													}}
													src={data?.Poster}
													fallback={errorImage}
												/>
											</Col>
											<Col>
												<div style={{ marginLeft: 8, padding: 8 }}>
													{Object.keys(data).map((subData, index) => {
														if (subData === 'Poster' || subData === 'imdbID') {
															return null;
														}
														return (
															<p key={index}>
																{subData} :{' '}
																<span>
																	{subData === 'Title'
																		? data[subData].substring(0, 10) + '...'
																		: data[subData]}
																</span>
															</p>
														);
													})}
												</div>
											</Col>
										</Row>
									</div>
								);
							})}
							{isLoadingSpin && (
								<div style={{ justifyContent: 'center', display: 'flex' }}>
									<Spin />
								</div>
							)}
						</div>
					</Content>
				</Col>
			</Row>
			<Modal
				title={'Detail poster Movie ' + data?.Title}
				visible={isShowModal}
				onOk={() =>
					setIsModalVisible({ ...isModalVisible, isShowModal: false })
				}
				onCancel={() =>
					setIsModalVisible({ ...isModalVisible, isShowModal: false })
				}>
				<div
					style={{
						borderRadius: 6,
						marginBottom: 14,
						border: `1px solid ${ColorBaseGrayEnum.gray400}`,
						cursor: 'pointer',
					}}>
					<Row>
						<Col>
							<Image
								width={'100%'}
								style={{
									borderTopLeftRadius: 6,
									borderBottomLeftRadius: 6,
									objectFit: 'cover',
								}}
								src={data?.Poster}
								fallback={errorImage}
							/>

							<div style={{ marginLeft: 8, padding: 8 }}>
								{data &&
									Object.keys(data).map((subData, index) => {
										if (subData === 'Poster' || subData === 'imdbID') {
											return null;
										}
										return (
											<p key={index}>
												{subData} : <span>{data[subData]}</span>
											</p>
										);
									})}
							</div>
						</Col>
					</Row>
				</div>
			</Modal>
		</Layout>
	);
};

export default App;
