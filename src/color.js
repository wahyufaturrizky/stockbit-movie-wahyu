export const ColorBaseGrayEnum = {
	gray100: '#FAFAFA',
	gray200: '#F5F5F5',
	gray300: '#EBEBEB',
	gray400: '#BDBDBD',
	gray500: '#9E9E9E',
	gray600: '#555555',
	gray700: '#212121',
};
